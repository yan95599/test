'use strict';

/* Controllers */

authz.controller('MyCtrl1',
  function($scope, $http) {
    $scope.name="pScope";
    var disabled_sts="color:#999999";
    var no_style="";
    $scope.checkedRsc = [false,false];
    $scope.dp_sts=disabled_sts;
    $scope.dlt_sts=disabled_sts;
    $scope.expt_sts=disabled_sts;
    $scope.expt_toggles="";
    $scope.rscDpBt = "disabled";
    $scope.rscRadio = "all";

    $scope.changeBtnStatus = function() {
      var i=0;
      var totalTrue=0;
      for(;i<$scope.checkedRsc.length;i++){
        if($scope.checkedRsc[i]){
          totalTrue++;
        }
      }

      if(totalTrue>0){
        $scope.expt_sts=no_style;
        $scope.expt_toggles="dropdown";
      }else{
        $scope.expt_sts=disabled_sts;
        $scope.expt_toggles=no_style;
      }

      if(totalTrue==$scope.checkedRsc.length){
        $scope.master=true;
      }else{
        $scope.master=false;
      }
      if(totalTrue==0){
        
        $scope.dlt_sts=disabled_sts;
      }else{
        $scope.dlt_sts=no_style;
      }
      if(totalTrue==1){
        $scope.dp_sts = no_style;
      }else{
         $scope.dp_sts =disabled_sts;
      }
    }

    $scope.masterClick=function(){
      var i=0;
      for(;i<$scope.checkedRsc.length;i++){
        $scope.checkedRsc[i]=$scope.master;
      }
      $scope.changeBtnStatus();
      }

    
    $scope.getRscData=function(){
       $http.get('http://127.0.0.1:9998/authz/rsc?callback=JSON_CALLBACK').success(function(data1) {
          $scope.rscs = data1;
        });
    }

    $scope.getRscData();

    $http.get('data/roles.json').success(function(data2) {
      $scope.roles = data2;
    });

    $http.get('data/users.json').success(function(data3) {
      $scope.users = data3;
    });

    $scope.openWindow=function() {
      var url='partials/authz-individual.html';
      var name='Resource';
      var iWidth=700;
      var iHeight=400;
      //window.screen.height获得屏幕的高，window.screen.width获得屏幕的宽
      var iTop = (window.screen.height - 30 - iHeight) / 2;
      var iLeft = (window.screen.width - 10 - iWidth) / 2;
      var params = 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=auto,resizeable=no,location=no,status=no';
      window.$windowScope = $scope;
      open(url, name, params);
    }

    $scope.addRsc=function(){
      $scope.rscs.push({"name":"name","value":"rsc","matching":"regex"});
    }

    $scope.testClick=function(){
      alert("test");
    }
  }
);

individualApp.controller('individualCtrl',
  function($scope, $http) {
    $scope.parentWindow = window.opener.$windowScope;
    var pData= angular.toJson($scope.parentWindow.rscs);
    $scope.addRscData=function(){
      // alert(angular.toJson($scope.parentWindow.rscs)-="]");
      // alert($scope.parentWindow.rscs.length);
      // alert(angular.isArray($scope.parentWindow.rscs));
      window.opener.$windowScope.rscs.push({"name":"name","value":"rsc","matching":"regex"});
      // srcArray[1]='{"name": "airport_getAirportInformationByContryName","resource": "{http://de}airport#getAirportInformationByContryName", "matching": "equal"}';
    }
});

