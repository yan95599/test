'use strict';

/* Controllers */

angular.module('registry.controllers', [])
    .controller('ServiceCtrl', ['$scope', 'AngularIssues', '$location', function ($scope, AngularIssues, $location) {
        $scope.data = {};
        $scope.selected={};
        $scope.policyList=$scope.policyList||[];

        AngularIssues.get('wsdl', true, '','', function (data) {
            $scope.data.issues = data.list;
        });

        AngularIssues.get('ws-policy',true,'','',function(data){
            $scope.policyList=data.list;
        })
        $scope.delete = function (idx) {
            var service_to_delete = $scope.data.issues[idx];
            $scope.data.issues.slice(idx, 1);
            AngularIssues.delete('wsdl', service_to_delete.id, function () {
                $location.path("#/service");
            })
        };

        $scope.populate=function(idx){
            var service_to_populate = $scope.data.issues[idx];
            AngularIssues.get('wsdl', false, service_to_populate.id, '',function (data) {
                $scope.selected = data;
            })
        }
    }])
    .controller('PolicyCtrl', ['$scope','AngularIssues',function ($scope,AngularIssues) {
        $scope.policies={};

        $scope.selected={};
        AngularIssues.get('ws-policy',true,'','',function(data){
            $scope.policies=data.list;
        })
        $scope.delete = function (idx) {
            var policy_to_delete = $scope.policies[idx];
            $scope.policies.slice(idx, 1);
            AngularIssues.delete('ws-policy', policy_to_delete.id, function () {
                $location.path("#/ws-policy");
            })
        };
        $scope.populate=function(idx){
            var policy_to_populate = $scope.policies[idx];
            AngularIssues.get('ws-policy', false, policy_to_populate.id, function (data) {
                $scope.selected = data;
            })
        }


    }])

    .controller('AddCtrl', ['$scope', 'AngularIssues', '$location','$routeParams', function ($scope, AngularIssues, $location,$routeParams) {
        var path = $routeParams.resourceType;
        $scope.save = function () {
            AngularIssues.save($routeParams.resourceType, '', $scope.resource, function () {
                $location.path(path);
            })
        }
    }])
    .controller('EditCtrl', ['$scope', '$routeParams', '$location', 'AngularIssues', function ($scope, $routeParams, $location, AngularIssues) {
        var path = $routeParams.resourceType;
        AngularIssues.get($routeParams.resourceType, false, $routeParams.id,'', function (data) {
            $scope.resource = data;
        })

        $scope.save = function () {
            AngularIssues.save($routeParams.resourceType, $routeParams.id, $scope.resource, function () {
                $location.path(path);
            })
        }

    }])
;

