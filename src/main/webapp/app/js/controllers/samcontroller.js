angular.module('myApp', [ 'angular-table' ]).controller('samcontroller',
		[ '$scope', '$http', function($scope, $http) {
			$scope.selectedEvent = {};
			$scope.events = [];

			// function to add events from the incoming JSON format
			$scope.addEvents = function(eventsToAdd) {
				if (typeof eventsToAdd != 'undefined') {
					$scope.events = [];
					var count = eventsToAdd.count;
					var aggregated = eventsToAdd.aggregated;

					for (var i = 0; i < count; i++) {
						$scope.events.push({
							date : new Date(aggregated[i].timestamp).format("yyyy-MM-dd h:mm:ss"),
							flowId : aggregated[i].flowID,
							providerHost : aggregated[i].providerHost,
							providerIP : aggregated[i].providerIP,
							consumerHost : aggregated[i].consumerHost,
							consumerIP : aggregated[i].consumerIP,
							transport : aggregated[i].transport,
							port: aggregated[i].port
						});
					}
				}
			};

			$scope.handleRowSelection = function(row) {
				$scope.selectedEvent = row;
				
			};

			// uses to get data from Runtime Sam
			$http({method : 'GET',
				url : '../../../AngularSample/sam'
			}).success(function(data) {
				$scope.eventsToAdd = data;
				$scope.addEvents($scope.eventsToAdd);
			});
			
			
			//Date Formatter
			Date.prototype.format = function(format)
			{
			  var o = {
			    "M+" : this.getMonth()+1, 
			    "d+" : this.getDate(),    
			    "h+" : this.getHours(),   
			    "m+" : this.getMinutes(), 
			    "s+" : this.getSeconds(), 
			    "q+" : Math.floor((this.getMonth()+3)/3),  
			    "S" : this.getMilliseconds() 
			  }

			  if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
			    (this.getFullYear()+"").substr(4 - RegExp.$1.length));
			  for(var k in o)if(new RegExp("("+ k +")").test(format))
			    format = format.replace(RegExp.$1,
			      RegExp.$1.length==1 ? o[k] :
			        ("00"+ o[k]).substr((""+ o[k]).length));
			  return format;
			}
			

			
} ]);
