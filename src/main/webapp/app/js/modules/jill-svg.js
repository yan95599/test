'use strict';


// Declare app level module which depends on filters, and services
var jsvg = angular.module('jsvg', []);

var ns = 'http://www.w3.org/2000/svg';

jsvg.controller('Ctrl', function($scope) {
  $scope.customer = {
    name: 'Naomi',
    address: '1600 Amphitheatre'
  };
});

jsvg.directive('jsvgCir', function($compile) {
  return {
    restrict: 'E',
    link: function(scope, lElement, lAttr) {
      var node = makeNode('circle', lElement, lAttr);
      node.setAttribute('fill', 'green');
      node.setAttribute('fill-opacity', '0.3');
      node.setAttribute('stroke-width', '1px');
      node.setAttribute('stroke', 'black');
      node.setAttribute('r', '18');

      // var newDad = node.cloneNode(true);
      // var children = lElement.children();
      // $(newDad).append(children);
      // lElement.replaceWith(newDad);
      lElement.replaceWith(node);
    }
  }
});

// <g id="esb-task1">
//     <rect fill="#7f7f7f" stroke-width="2" y="125" x="240" rx="10" ry="10" width="100" height="50" id="svg_1" stroke-linejoin="bevel" stroke-linecap="round" opacity="0.25" stroke="black"/>
//     <text transform="matrix(1.02069 0 0 1 -161.225 0)" stroke="black" xml:space="preserve" text-anchor="middle" font-family="serif" font-size="24" id="svg_8" y="160" x="441.5874" opacity="0.25" stroke-linecap="round" stroke-linejoin="bevel" stroke-dasharray="null" stroke-width="0" fill="#000000">task1</text>
//   </g>
jsvg.directive('jsvgRec', function($compile) {
  return {
    restrict: 'E',
    link: function(scope, lElement, lAttr) {
      //create svg group node "g"
      var id = lAttr["id"];
      var cx = lAttr["cx"];
      var cy = lAttr["cy"];
      var textValue = lAttr["text"];
      var groupNode = makeNode('g', lElement, lAttr);

      //test innerHTML
      var x = cx - 50;
      var y = cy - 25;
      var textX = parseInt(x) + 50;
      var textY = parseInt(y) + 35;
      groupNode.innerHTML =
      //create rect
      '<rect fill="#7f7f7f" stroke-width="2" y="' + y + '" x="' + x +
        '" rx="10" ry="10" width="100" height="50" id="svg_1" stroke-linejoin="bevel" stroke-linecap="round" opacity="0.25" stroke="black"/>'
      //create text
      + '<text  stroke="black" xml:space="preserve" text-anchor="middle" font-family="serif" font-size="24" id="svg_8" y="' + textY + '" x="' + textX + '" opacity="0.25" stroke-linecap="round" stroke-linejoin="bevel" stroke-dasharray="null" stroke-width="0" fill="#000000">' +
        textValue + '</text>';

      lElement.replaceWith(groupNode);
    }
  }
});
/*<g id="esb-branch">
    <rect stroke="black" transform="rotate(-45 295 230)" id="svg_9" height="30" width="30" y="215" x="270" opacity="0.25" stroke-linecap="round" stroke-linejoin="bevel" stroke-dasharray="null" stroke-width="0" fill="#000000"/>
    <line stroke="black" stroke-width="2" id="svg_11" y2="235" x2="290" y1="235" x1="280" opacity="0.25" stroke-linecap="round" stroke-linejoin="bevel" stroke-dasharray="null" fill="none"/>
    <line id="svg_13" y2="240" x2="285" y1="230" x1="285" opacity="0.25" stroke-linecap="round" stroke-linejoin="bevel" stroke-dasharray="null" stroke-width="2" stroke="black" fill="none"/>

  </g>*/

jsvg.directive('jsvgBranch', function($compile) {
  return {
    restrict: 'E',
    link: function(scope, lElement, lAttr) {

      //create svg group node "g"
      var groupNode = makeNode('g', lElement, lAttr);

      //create rect node
      var cx = lAttr['cx'];
      var cy = lAttr['cy'];
      var x = cx - 15;
      var y = cy - 15;
      var rotateStr = "rotate(-45 " + cx + " " + cy + ")";
      var rectStr = '<rect stroke="black" transform="' + rotateStr + '" id="svg_9" height="30" width="30" y="' + y + '" x="' + x + '" opacity="0.25" stroke-linecap="round" stroke-linejoin="bevel" stroke-dasharray="null" stroke-width="0" fill="#000000"/>';

      var x1 = cx - 10;
      var x2 = parseInt(cx) + 10;
      var lineHStr = '<line stroke="black" stroke-width="2" id="svg_11" y2="' + cy + '" x2="' + x2 + '" y1="' + cy + '" x1="' + x1 + '" opacity="0.25" stroke-linecap="round" stroke-linejoin="bevel" />';

      var y1 = cy - 10;
      var y2 = parseInt(cy) + 10;
      var lineVStr = '<line id="svg_13" y2="' + y2 + '" x2="' + cx + '" y1="' + y1 + '" x1="' + cx + '" opacity="0.25" stroke-linecap="round" stroke-linejoin="bevel"  stroke-width="2" stroke="black" />';

      //append rect and line node into the group
      groupNode.innerHTML = rectStr + lineHStr + lineVStr;
      lElement.replaceWith(groupNode);
    }
  }
});

// <path d="M286,80 L286,115"
//       style="stroke: #6666ff; stroke-width: 1px; fill: none;
//                    marker-end: url(#markerArrow);"
//         />
jsvg.directive('jsvgPath', function() {
  return {
    restrict: 'E',
    link: function(scope, iElement, iAttrs) {
      //get fromShape and toShape.
      var from = iAttrs["from"];
      var to = iAttrs["to"];
      var fromShape = $("#" + from);
      var toShape = $("#" + to);

      //get basic information: shape type, center point.
      var fcx = fromShape.attr("cx");
      var fcy = fromShape.attr("cy");
      var fType = fromShape.attr("type");

      var tcx = toShape.attr("cx");
      var tcy = toShape.attr("cy");
      var tType = toShape.attr("type");

      console.log("fcx:"+fcx+", fcy:"+fcy+"; tcx:"+tcx+", tcy:"+tcy);
      var cirIncrement =20, rectIncrementS=27, rectIncrementL=52, branchIncrement=25;
      var markerArrowIncrement=8;
      var pathD;
      //compute the path
      //horizonal, vertical line, oblique line
      if (fcx==tcx ) {//vertical line
        
        if(fcy-tcy>0){//up line
          console.log("fcx==tcx && fcy<tcy");
          var  my, ly;
          if(fType =='cir'){
            my= fcy-cirIncrement;
          }else if(fType=='rect'){
            my=fcy-rectIncrementS;
          }else if(fType=='branch'){
            my=fcy-branchIncrement;
          }

          if(tType=='cir'){
            ly=parseInt(tcy)+cirIncrement+markerArrowIncrement;
          }else if(tType=='rect'){
            ly=parseInt(tcy)+rectIncrementS+markerArrowIncrement;
          }else if(tType=='branch'){
            ly=parseInt(tcy)+branchIncrement+markerArrowIncrement;
          }
          pathD="M"+fcx+","+my+" L"+fcx+","+ly;
        }else if(fcy-tcy<0){ //down line
          var  my, ly;
          if(fType =='cir'){
            my= parseInt(fcy)+cirIncrement;
          }else if(fType=='rect'){
            my=parseInt(fcy)+rectIncrementS;
          }else if(fType=='branch'){
            my=parseInt(fcy)+branchIncrement;
          }

          if(tType=='cir'){
            ly=tcy-cirIncrement-markerArrowIncrement;
          }else if(tType=='rect'){
            ly=tcy-rectIncrementS-markerArrowIncrement;
          }else if(tType=='branch'){
            ly=tcy-branchIncrement-markerArrowIncrement;
          }
          pathD="M"+fcx+","+my+" L"+fcx+","+ly;
        }
       
      }else if(tcy==fcy){//horizonal line
        var mx, lx;
        if(fcx - tcx <0){//from left to right
          console.log("left to right");
          if(fType=='cir'){
            mx= parseInt(fcx)+cirIncrement;
          }else if(fType=='rect'){
            mx=parseInt(fcx)+rectIncrementL;
          }else if(fType=='branch'){
            mx=parseInt(fcx)+branchIncrement;
          }

          if(tType=='cir'){
            lx=tcx-cirIncrement-markerArrowIncrement;
          }else if(tType=='rect'){
            lx=tcx-rectIncrementL-markerArrowIncrement;
          }else if(tType=='branch'){
            lx=tcx-branchIncrement-markerArrowIncrement;
          }
        }else if(fcx -tcx>0){//from right to left
         console.log("right to left");
          if(fType=='cir'){
            mx= fcx-cirIncrement;
          }else if(fType=='rect'){
            mx=fcx-rectIncrementL;
          }else if(fType=='branch'){
            mx=fcx-branchIncrement;
          }

          if(tType=='cir'){
            lx=parseInt(tcx)+cirIncrement+markerArrowIncrement;
          }else if(tType=='rect'){
            lx=parseInt(tcx)+rectIncrementL+markerArrowIncrement;
          }else if(tType=='branch'){
            lx=parseInt(tcx)+branchIncrement+markerArrowIncrement;
          }
        }
         pathD="M"+mx+","+fcy+" L"+lx+","+fcy;
      }else{//oblique line
        if(tcy-fcy>0){//down line
            var mx, my, lx, ly;
            mx=fcx;
            if(fType=='cir'){
              my=parseInt(fcy)+cirIncrement;
            }else if(fType=='rect') {
              my=parseInt(fcy)+rectIncrementS;
            }else if(fType=='branch'){
              my=parseInt(fcy)+branchIncrement;
            }

            if(tcx-fcx>0){//left to right
              lx=tcx-10;
            }else{//right to left
              lx=parseInt(tcx)+10;
            }
            if(tType=='cir'){
              ly=tcy-cirIncrement-markerArrowIncrement;
            }else if(tType=='rect'){
              ly=tcy-rectIncrementS-markerArrowIncrement;
            }else if(tType=='branch'){
              ly=tcy-branchIncrement-markerArrowIncrement;
            }
            pathD="M"+mx+","+my+" L"+lx+","+ly;
        }else if(fcy - tcy >0){//up line
       
            var mx, my, lx, ly;
            mx=fcx;
            if(fType=='cir'){
              my=fcy-cirIncrement;
            }else if(fType=='rect') {
              my=fcy-rectIncrementS;
            }else if(fType=='branch'){
              my=fcy-branchIncrement;
            }

            if(tcx-fcx>0){//left to right
              lx=tcx-10;
            }else{//right to left
              lx=parseInt(tcx)+10;
            }
            if(tType=='cir'){
              ly=parseInt(tcy)+cirIncrement+markerArrowIncrement;
            }else if(tType=='rect'){
              ly=parseInt(tcy)+rectIncrementS+markerArrowIncrement;
            }else if(tType=='branch'){
              ly=parseInt(tcy)+branchIncrement+markerArrowIncrement;
            }
            pathD="M"+mx+","+my+" L"+lx+","+ly;
        }
      };

      var node = makeNode('path', iElement, iAttrs);
      node.setAttribute('style', 'stroke: #6666ff; stroke-width: 1px; fill: none; marker-end: url(#markerArrow);');
      node.setAttribute('d',pathD);
      iElement.replaceWith(node);

    }
  };
});

/* Create a shape node with the given settings. */
function makeNode(name, element, lAttr) {
  // var ns = 'http://www.w3.org/2000/svg';
  var node = document.createElementNS(ns, name);
  for (var attribute in lAttr) {
    var value = lAttr[attribute];
    if (value !== null && value !== null && !attribute.match(/\$/) &&
      (typeof value !== 'string' || value !== '')) {
      node.setAttribute(attribute, value);
    }
  }
  return node;
}