'use strict';


// Declare app level module which depends on filters, and services
//../../../AngularSample/authz'
//http://localhost:8090/authz/authz/test/

var qaApp=angular.module('qaApp', []);

qaApp.controller('qaCtrl',
  function($scope, $http) {
    $http({method : 'GET',
        url : 'http://127.0.0.1:9998/authz/o?callback=JSON_CALLBACK'
      }).success(function(data) {
        $scope.qas = data;
      }).error(function(data, status, headers, config) {
        alert("error:"+data+",status:"+status+",headers:"+headers+",config:"+config);
      });

    
    // $http.jsonp('http://127.0.0.1:9998/authz/p?callback=JSON_CALLBACK').success(function(data5) {
    //    alert(data5);
    //   // $scope.qas = data5;
    // });

    $http.get('http://localhost:9998/helloworld').success(function(data6) {
      $scope.hello = data6;
    });

	$http.get('http://rest-service.guides.spring.io/greeting').success(function(data4) {
      $scope.greeting = data4;
    });

});