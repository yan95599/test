var app = angular.module('plunker', []);

app.controller('MainCtrl', function($scope) {
  $scope.squares = [{
    path: "M0,0L50,0L50,50L0,50z",
    color: 'blue',
    order: 2
  }, {
    path: "M0,0L75,0L75,75L0,75z",
    color: 'purple',
    order: 1
  }];

});

app.directive('hello', function($compile) {
  return {
    restrict: 'E',
    transclude: true,
    compile: function(tElement, tAttr, transclude) {
      return function(scope, element) {
        transclude(scope.$parent, function(clone) {
          var newElement = $('<div>Hello </div>');
          newElement.append(clone);
          element.replaceWith(newElement);
        });
      }
    }
  }
});

app.directive('world', function() {
  return {
    restrict: 'E',
    compile: function(tElement, tAttr) {
      return function(scope, element) {
        element.replaceWith('<span>World</span>');


      }
    }
  }
}) 


app.directive('group', function($compile) {
  return {
    restrict: 'E',
    link: function(scope, lElement, lAttr) {
      var path = makeNode('g', lElement, lAttr);
      var newDad = path.cloneNode(true);
      var children = lElement.children();
      $(newDad).append(children);
      lElement.replaceWith(newDad);
    }
  }
});

app.directive('shape', function($timeout) {
  return {
    restrict: 'E',
    link: function(scope, lElement, lAttr) {
      var path = makeNode('path', lElement, lAttr);
      var newGuy = path.cloneNode(true);
      $timeout(function() {
        lElement.replaceWith(newGuy);
      })
      console.log('Replacing ', lElement, ' with ', newGuy);
    }
  }
})

/* Create a shape node with the given settings. */
function makeNode(name, element, settings) {
  var ns = 'http://www.w3.org/2000/svg';
  var node = document.createElementNS(ns, name);
  for (var attribute in settings) {
    var value = settings[attribute];
    if (value !== null && value !== null && !attribute.match(/\$/) &&
      (typeof value !== 'string' || value !== '')) {
      node.setAttribute(attribute, value);
    }
  }
  return node;
}
app.directive('jsvgCir', function($compile) {
  return {
    restrict: 'E',
    link: function(scope, lElement, lAttr) {
      /*var path = makeNode('g', lElement, lAttr);*/

      var ns = 'http://www.w3.org/2000/svg';
      var node = document.createElementNS(ns, 'circle');
      node.setAttribute('id','esb-start');
      node.setAttribute('fill','green');
      node.setAttribute('fill-opacity','0.3');
      node.setAttribute('stroke-width','1px');
      node.setAttribute('stroke','black');
      node.setAttribute('r','17');
      node.setAttribute('cy','60');
      node.setAttribute('cx','200');
      

      var newDad = node.cloneNode(true);
      var children = lElement.children();
      $(newDad).append(children);
      lElement.replaceWith(newDad);
    }
  }
});
app.directive('myCustomer', function() {
    return {
      restrict: 'E',
      template: '<svg style="height:300px"><jsvg-cir></jsvg-cir></svg>',
      replace: true
    };
  });

app.directive('jsvgCir', function($compile) {
    return {
        restrict: 'EA',
        template: '<circle id="esb-start" fill="green" fill-opacity="0.3" stroke-width="1px" stroke="black" r="17" cy="60" cx="286"/>',
        replace: true,
    };
});