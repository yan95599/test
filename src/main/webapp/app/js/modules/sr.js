'use strict';


// Declare app level module which depends on filters, and services
angular.module('registry', ['ngRoute', 'registry.filters', 'registry.services', 'registry.directives', 'registry.controllers','ui.bootstrap']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/service', {templateUrl: 'partials/service.html', controller: 'ServiceCtrl'});
        $routeProvider.when('/ws-policy', {templateUrl: 'partials/policy.html', controller: 'PolicyCtrl'});
        $routeProvider.when('/new/:resourceType', {templateUrl: 'partials/detail.html', controller: 'AddCtrl'});
        $routeProvider.when('/edit/:resourceType/:id',{templateUrl:'partials/detail.html',controller:'EditCtrl'});
        $routeProvider.otherwise({redirectTo: '/service'});
    }])
;




