'use strict';


// Declare app level module which depends on filters, and services
var authz=angular.module('authz', [
  'authz.filters',
  'authz.services',
  'authz.directives'
]);
// config(['$routeProvider', function($routeProvider) {
//   $routeProvider.when('/view1', {templateUrl: 'partials/authz-main.html', controller: 'MyCtrl1'});
//   $routeProvider.when('/individual', {templateUrl: 'partials/authz-main.html', controller: 'MyCtrl2'});
//   $routeProvider.otherwise({redirectTo: '/view1'});
// }]);

var individualApp=angular.module('individualApp', []);
