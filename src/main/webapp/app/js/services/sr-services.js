'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('registry.services', ['ngResource'])
    .factory('AngularIssues', ['$http', function ($http) {
        return{
            get: function (type, collection, id,name, callback) {
                $http.get('/AngularSample/wsdl?type=' + type + '&collection=' + collection + '&id=' + id+'&name='+name
                    ).success(function (data, status) {
                        callback(data);
                    })
            },
            save: function (type, id, dataToSave, callback) {
                $http.post('/AngularSample/wsdl?type=' + type + '&id=' + id, dataToSave)
                    .success(function (data, status) {
                        callback();
                    })
            },
            delete: function (type, id, callback) {
                $http.delete('/AngularSample/wsdl?type=' + type + '&id=' + id)
                    .success(function (data, status) {
                        callback();
                    })
            }
        }
    }])
    .service('modalService', function ($modal) {
        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: '/app/partials/modal.html'
        };

        var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Proceed?',
            bodyText: 'Perform this action?'
        };

        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function (customModalDefaults, customModalOptions) {
            var tempModalDefaults = {};
            var tempModalOptions = {};

            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.ok = function (result) {
                        $modalInstance.close(result);
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                    };
                }
            }

            return $modal.open(tempModalDefaults).result;
        };

    })

;
