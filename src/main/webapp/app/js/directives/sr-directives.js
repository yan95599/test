'use strict';

/* Directives */


angular.module('registry.directives', []).
    directive('treeGrid', ['$compile', 'modalService', 'AngularIssues', function ($compile, modalService, AngularIssues) {
        return{
            restrict: "E",
            link: function (scope, element, attrs) {

                var node = scope.selected.tree || [];


                var findChildren = function (selected) {
                    var depth = selected.data("depth");
                    return selected.nextUntil($('tr').filter(function () {
                        return $(this).data('depth') <= depth;
                    }));
                };

                scope.notify = function (data) {
                    var td = $('#' + data + ' td:first');
                    // Node selected highlight
                    // Remove highlight from previously selected node
                    if (scope.currentNode != td && scope.currentNode != undefined) {
                        scope.currentNode.removeClass('selected');
                    }
                    if (!td.hasClass('selected')) {
                        td.addClass('selected');
                    }
                    ;
                    scope.currentNode = td;
                };

                scope.toggle = function (id) {
                    var selected = $('#' + id);
                    var children = findChildren(selected);
                    var i = $('#' + id + ' i:first');

                    children.each(function () {
                        // filter out nodes which are already folded
                        var itag = $(this).find('i:first');
                        var subChildren = findChildren($(this));
                        if (itag.hasClass('icon-plus')) {
                            children = children.not(subChildren);
                        }
                    });
                    // flip 'plug' and 'minus' icons.
                    if (i.hasClass('icon-plus')) {
                        i.removeClass('icon-plus').addClass('icon-minus');
                        children.show();
                    } else {
                        i.removeClass('icon-minus').addClass('icon-plus');
                        children.hide();
                    }
                };
                scope.view = function (policyName) {
                    AngularIssues.get('ws-policy', false, '', policyName, function (data) {

                        console.log(data.content);

                        var modalOptions = {
                            closeButtonText: 'Close',
                            headerText: 'Policy Content',
                            bodyText: data.content
                        };

                        modalService.showModal({}, modalOptions)
                    })

                };

                var appendChildren = function (node) {
                    var whole = '';
                    for (var i = 0; i < node.length; i++) {
                        var head = node[i];

                        var hasChildren = head.children.length > 0;
                        var partial = '<tr class="expand level' + head.level + '" ng-show=' + !head.isCollapsed +
                            ' id="' + head.id + '" data-depth="' + head.level + '" >'
                            + '<td ng-click=notify("' + head.id + '")>';
                        if (hasChildren) {
                            partial = partial +
                                '<i class="icon-minus" ' +
                                'ng-show=' + !head.isCollapsed + ' ' +
                                'ng-click="toggle(' + head.id + ')">' +
                                '</i>';
                        } else {
                            partial = partial + '<i class=""></i>';
                        }

                        partial = partial + head.label + '</td><td>';

                        head.assignments = head.assignments || [];
                        for (var j = 0; j < head.assignments.length; j++) {
                            partial = partial +
                                '<span>' +
                                '<button class="btn"><i class="icon-file" ng-click="view(' + head.assignments[j].policyName + ')"></i>View</button>' +
                                '<button class="btn"><i class="icon-remove"></i>Unassign</button>' +
                                '<span>' + head.assignments[j].policyName + '</span>' +
                                '</span>';
                        }

                        partial = partial + '</td></tr>';

                        if (hasChildren) {
                            partial = partial + appendChildren(head.children);
                        }
                        whole = whole + partial;
                    }
                    return whole;
                };

                var template = appendChildren(node);

                element.html('').append($compile('<table table-bordered >' + template + '</table>')(scope));
//                watch seleted.tree, once it's changed, update the tree table view
                scope.$watch('selected.tree', function (value) {
                    scope.selected.tree = scope.selected.tree || [];
                    template = appendChildren(scope.selected.tree);
                    element.html('').append($compile('<table class="table table-bordered">' + template + '</table>')(scope));
                })

            }
        }
    }])
