package com.talend.example.authz;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.core.spi.factory.ResponseBuilderImpl;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import org.apache.commons.io.IOUtils;

@Path("/authz")
public class AuthzService {
	 
		@GET
		@Path("/test/")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getMsg(){
			String inputContent = null;
			try {   
	            String file="F:\\jill\\file.txt";
	            FileReader reader=new FileReader(file);
	            StringWriter swriter = new StringWriter();
	            IOUtils.copy(reader, swriter);
	            reader.close();
	            inputContent = "["+swriter.toString()+"]";
				System.out.println(inputContent);
	        } catch (IOException e) {   
	            e.printStackTrace();   
	        }   
	        ResponseBuilderImpl resb = new ResponseBuilderImpl();
	 		resb.header("Access-Control-Allow-Credentials", true);
	 		resb.status(200);
	 		resb.entity(inputContent);
	 		return resb.build();
		}

		
		@GET
		@Path("/qa/")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getQaMsg(){
			String inputContent = null;
			try {   
	            String file="F:\\jill\\file.txt";
	            FileReader reader=new FileReader(file);
	            StringWriter swriter = new StringWriter();
	            IOUtils.copy(reader, swriter);
	            reader.close();
	            inputContent = "["+swriter.toString()+"]";
				System.out.println(inputContent);
	        } catch (IOException e) {   
	            e.printStackTrace();   
	        }   
	        ResponseBuilderImpl resb = new ResponseBuilderImpl();
	 		resb.header("Access-Control-Allow-Credentials", true);
	 		resb.header("Access-Control-Allow-Origin", "*");
	 		resb.status(200);
	 		resb.entity(inputContent);
	 		return resb.build();
		}
		
}
