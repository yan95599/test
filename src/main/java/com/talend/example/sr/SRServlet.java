package com.talend.example.sr;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.wsdl.Binding;
import javax.wsdl.BindingOperation;
import javax.wsdl.Definition;
import javax.wsdl.Port;
import javax.wsdl.Service;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.talend.esb.registry.atom.service.client.AtomPubRegistryClient;
import org.talend.esb.registry.atom.service.client.Resource;
import org.talend.esb.registry.atom.service.client.ResourceCollection;
import org.talend.esb.registry.atom.service.client.ResourceCollectionPage;
import org.talend.esb.registry.atom.service.client.impl.AtomPubRegistryClientFactory;
import org.talend.esb.registry.common.policy.ExternalPolicyAttachmentWsdlElementIdentifier;
import org.talend.esb.registry.common.policy.ExternalPolicyAttachmentWsdlElementIdentifierBuilder;
import org.talend.esb.rest.service.client.RestClient;
import org.talend.esb.rest.service.client.RestClientConfig;
import org.talend.esb.rest.service.client.impl.RestClientFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Servlet implementation class SRServlet
 */
public class SRServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SRServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/atom+xml;type=entry");
        PrintWriter printWriter = response.getWriter();

        // wsdl, policy, or policy-attachment
        String type = request.getParameter("type");
        boolean collection = request.getParameter("collection").equals("true");
        String id = (String) request.getParameter("id");
        String name = request.getParameter("name");

        // Get wsdl resource collection
        ResourceCollection resourceCollection = getResourceCollection(type);
        ResourceCollectionPage resColPage = resourceCollection.fetchPage(true);

        JSONObject responseObject = new JSONObject();

        if (collection) {
            // Construct JSON list
            List<JSONObject> jsonList = new ArrayList<JSONObject>();
            for (Resource res : resColPage.getResources()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("title", res.getTitle());
                jsonObject.put("id", res.getId());
                jsonObject.put("summary", res.getSummary());
                jsonObject.put("updated", res.getUpdated());
                jsonList.add(jsonObject);
            }
            responseObject.put("list", jsonList);

        } else {
            // Fetch resource collection page
            Iterable<Resource> iterator = resColPage.getResources();

            try {

                for (Resource res : iterator) {
                    if (id != null && res.getId().equals(id) || name != null && res.getName().equals(name)) {
                        responseObject.put("id", id);
                        responseObject.put("title", res.getTitle());
                        responseObject.put("summary", res.getSummary());
                        responseObject.put("content", new String(res.getSource()));
                        responseObject.put("tree", parse(res.getSource()));
                        lookupPolicy(res, responseObject);
                        System.out.println("========================================================");
                        System.out.println(responseObject.toString());
                        System.out.println("========================================================");
                    }
                }
            } catch (Exception e) {
                processException(e);
            }

        }

        printWriter.write(responseObject.toString());

    }

    private void lookupPolicy(Resource res, JSONObject wsdl) throws Exception {
        ResourceCollection resourceCollection = getResourceCollection("ws-policy-attach");
        ResourceCollectionPage resColPage = resourceCollection.fetchPage(true);
        Iterable<Resource> iterator = resColPage.getResources();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        ExternalPolicyAttachmentWsdlElementIdentifier epawei = null;
        JSONArray services = wsdl.getJSONArray("tree").getJSONObject(0).getJSONArray("children");
        for (Resource attach : iterator) {
            Document doc = builder.parse(attach.getSourceAsStream());
            NodeList list = doc.getFirstChild().getChildNodes();

            String applies = "";
            for (int i = 0; i < list.getLength(); i++) {
                JSONObject item = new JSONObject();
                Node node = list.item(i);
                if (node.getNodeName().equals("wsp:AppliesTo")) {
                    applies = node.getFirstChild().getTextContent();
                }
                if (node.getNodeName().equals("wsp:PolicyReference")) {
                    String policyName = node.getAttributes().getNamedItem("URI").getNodeValue();
                    item.put("policyName", policyName);
                    item.put("applyTo", applies);
                    epawei = ExternalPolicyAttachmentWsdlElementIdentifierBuilder.build(applies);
                    for (int j = 0; j < services.length(); j++) {
                        if (epawei.appliesToService(buildQname(URLDecoder.decode(
                                (String) services.getJSONObject(j).getString("service-qname"), "utf-8")))) {
                            services.getJSONObject(j).getJSONArray("assignments").put(item);
                        }
                        JSONArray bindings = services.getJSONObject(j).getJSONArray("children");
                        for (int x = 0; x < bindings.length(); x++) {
                            JSONArray operations = bindings.getJSONObject(x).getJSONArray("children");
                            for (int k = 0; k < operations.length(); k++) {
                                if (epawei.appliesToBindingOperation(buildQname(URLDecoder.decode((String) operations
                                        .getJSONObject(k).get("binding-qname"), "utf-8")), buildQname(URLDecoder.decode(
                                        (String) operations.getJSONObject(k).get("operation-qname"), "utf-8")))) {
                                    if (operations.getJSONObject(k).has("assignments")) {
                                        operations.getJSONObject(k).getJSONArray("assignmentse").put(item);
                                    } else {
                                        operations.getJSONObject(k).put("assignments", new JSONArray().put(item));
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }

    }

    private QName buildQname(String qname) {

        QName result = null;
        qname = qname.substring(1);
        String[] splitResult = qname.split("}");
        result = new QName(splitResult[0], splitResult[1]);

        return result;
    }

    private JSONArray parse(byte[] source) throws Exception {

        JSONObject namespace = new JSONObject();
        WSDLFactory wsdlFactory = WSDLFactory.newInstance();
        WSDLReader wsdlReader = wsdlFactory.newWSDLReader();
        Definition def = wsdlReader.readWSDL(null, getInputSource(source));
        @SuppressWarnings("unchecked")
        Map<QName, Service> servicesMap = def.getServices();
        String rootNS = def.getTargetNamespace();
        int i = 0;
        namespace.put("id", i++);
        namespace.put("label", def.getTargetNamespace());
        namespace.put("type", "NS");
        namespace.put("level", "0");
        namespace.put("isCollapsed", false);
        JSONArray services = new JSONArray();

        for (QName key : servicesMap.keySet()) {
            // service
            JSONObject serviceJson = new JSONObject();
            serviceJson.put("id", i++);
            serviceJson.put("label", key.getLocalPart());
            serviceJson.put("service-qname", URLEncoder.encode(key.toString(), "utf-8"));
            serviceJson.put("type", "SERVICE");
            serviceJson.put("level", "1");
            serviceJson.put("isCollapsed", false);
            JSONArray bindings = new JSONArray();

            for (Object keyPort : servicesMap.get(key).getPorts().keySet()) {
                // binding
                Binding binding = ((Port) servicesMap.get(key).getPorts().get(keyPort)).getBinding();
                JSONObject bindingJson = new JSONObject();
                bindingJson.put("id", i++);
                bindingJson.put("label", binding.getQName().getLocalPart());
                bindingJson.put("service-qname", URLEncoder.encode(binding.getQName().toString(), "utf-8"));
                bindingJson.put("type", "BINDING");
                bindingJson.put("level", "2");
                bindingJson.put("isCollapsed", false);
                JSONArray operations = new JSONArray();
                List<BindingOperation> bindingOperations = binding.getBindingOperations();
                for (BindingOperation bo : bindingOperations) {
                    // operation
                    JSONObject operationJson = new JSONObject();
                    operationJson.put("id", i++);
                    operationJson.put("label", bo.getOperation().getName());
                    operationJson.put("service-qname", URLEncoder.encode(key.toString(), "utf-8"));
                    operationJson.put("binding-qname", URLEncoder.encode(binding.getQName().toString(), "utf-8"));
                    operationJson.put("operation-qname", "{" + rootNS + "}" + bo.getOperation().getName());
                    operationJson.put("type", "OPERATION");
                    operationJson.put("level", "3");
                    operationJson.put("isCollapsed", false);
                    operationJson.put("children", new JSONArray());
                    operations.put(operationJson);
                }
                bindingJson.put("children", operations);

                bindings.put(bindingJson);
            }
            serviceJson.put("children", bindings);

            services.put(serviceJson);
        }

        namespace.put("children", services);

        JSONArray tree = new JSONArray().put(namespace);
        return tree;
    }

    private InputSource getInputSource(byte[] source) {
        InputStream stream = new ByteArrayInputStream(source);
        Reader reader = null;
        try {
            reader = new InputStreamReader(stream, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        InputSource is = new InputSource(reader);
        is.setEncoding("UTF-8");
        return is;
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // wsdl, policy, or policy-attachment
        String type = request.getParameter("type");
        String id = request.getParameter("id");

        InputStream inputStream = request.getInputStream();
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        String inline = "";
        while ((inline = inputReader.readLine()) != null) {
            sb.append(inline);
        }

        String jsonData = sb.toString();
        String title = null;
        String content = null;
        String summary = null;

        try {
            // Converting incoming string to Json object.
            JSONObject jsonObject = new JSONObject(jsonData);
            // Getting data.
            title = jsonObject.getString("title");
            content = jsonObject.getString("content");
            summary = jsonObject.getString("summary");

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }

        // Get wsdl resource collection
        ResourceCollection resourceCollection = getResourceCollection(type);
        // Build resource
        Resource resource = resourceCollection.buildResource(id == null || "".equals(id) ? "" : id);
        resource.setSource(content.getBytes());
        resource.setTitle(title);
        resource.setSummary(summary);
        // Create or update resource
        if (id == null || "".equals(id)) {
            resourceCollection.create(resource);
        } else {
            resourceCollection.update(resource);
        }

    }

    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("type");
        String id = req.getParameter("id");
        try {
            deleteService(type, id);
        } catch (Exception e) {
            processException(e);
        }
    }

    private void deleteService(String type, String id) throws Exception {

        ResourceCollection resourceCollection = getResourceCollection(type);
        resourceCollection.delete(id);

    }

    private ResourceCollection getResourceCollection(String type) {
        try {

            // Create Rest Config
            RestClientConfig config = new RestClientConfig();
            config.setUrl(new URL("http://localhost:8040/services/registry/admin"));
            // Create rest client
            RestClient restClient = RestClientFactory.getClient(config);
            // Create atom pub registry client
            AtomPubRegistryClient atomPubRegistryClient = AtomPubRegistryClientFactory.getClient(restClient);
            // Get wsdl resource collection
            ResourceCollection resourceCollection = atomPubRegistryClient.getResourceCollection(type);

            return resourceCollection;
        } catch (Exception e) {
            processException(e);
        }

        return null;
    }

    private void processException(Exception e) {
        System.out.println(e.getLocalizedMessage());
        e.printStackTrace();

    }

}
