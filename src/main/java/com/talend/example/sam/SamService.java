package com.talend.example.sam;

import java.net.URL;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.core.spi.factory.ResponseBuilderImpl;

@Path("/sam")
public class SamService {
	 
		@GET
		@Path("/")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getMsg(String msg){
			Object object =null;
	 		try{
	 			URL url = new URL("http://localhost:8040/services/sam/list");
	 			url.openConnection();
	 			object = url.getContent();
	 		}catch (Exception e){
	 			System.out.println(e.getLocalizedMessage());
	 			return Response.status(500).entity("error").build();
	 		}
	 		ResponseBuilderImpl resb = new ResponseBuilderImpl();
	 		resb.header("Access-Control-Allow-Credentials", true);
	 		resb.status(200);
	 		resb.entity(object);
	 		return resb.build();
		}
		
		
}
